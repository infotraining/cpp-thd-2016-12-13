#include <iostream>
#include <string>
#include <thread>
#include <mutex>

using namespace std;

timed_mutex mtx;

void background_worker(int id, chrono::milliseconds timeout)
{
	cout << "THD#" << id << " is waiting for a mutex..." << endl;

	unique_lock<timed_mutex> lk{ mtx, try_to_lock };

	if (!lk)
	{
		do
		{
			cout << "THD#" << id << " doesn't own a lock..."
				<< " Attempt to acquire a mutex..." << endl;
		} while (!lk.try_lock_for(timeout));
	}

	cout << "Start THD#" << id << endl;

	for(int i = 0; i < 10; ++i)
	{
		cout << "THD#" << id << " is working..." << endl;
		this_thread::sleep_for(1s);
	}
}

int main()
{
	thread thd1{ &background_worker, 1, 500ms };
	thread thd2{ &background_worker, 2, 750ms };
	thd1.join();
	thd2.join();

	system("PAUSE");
}
