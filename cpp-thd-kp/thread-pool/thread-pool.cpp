#include <iostream>
#include <string>
#include <thread>
#include "thread_safe_queue.hpp"
#include <random>

using namespace std;

using Task = std::function<void()>;

class ThreadPool
{
	ThreadSafeQueue<Task> q_;
	vector<thread> threads_;
public:
	ThreadPool(size_t size) : threads_(size)
	{
		for(size_t i = 0; i < threads_.size(); ++i)
		{
			threads_[i] = thread{ [this] { run(); } };
		}
	}

	ThreadPool(const ThreadPool&) = delete;
	ThreadPool& operator=(const ThreadPool&) = delete;

	~ThreadPool()
	{
		q_.done();

		for (auto& t : threads_)
			t.join();
	}

	void submit(Task task)
	{
		q_.push(task);
	}

private:
	const static Task end_of_work_;

	void run()
	{
		while(true)
		{
			Task task;

			if (!q_.pop(task))
				return;

			task();
		}
	}
};

void background_task(int id)
{
	cout << "BT#" << id << " has started..." << endl;

	random_device rd;
	mt19937_64 rnd(rd());
	uniform_int_distribution<> distr(200, 3000);

	this_thread::sleep_for(chrono::milliseconds(distr(rnd)));

	cout << "BT#" << id << " has finished..." << endl;
}

int main()
{
	{
		ThreadPool thread_pool(8);

		thread_pool.submit([] { background_task(1); });
		thread_pool.submit([] { background_task(2); });

		for (int i = 3; i <= 20; ++i)
			thread_pool.submit([i] { background_task(i); });
	}

	system("PAUSE");
}
