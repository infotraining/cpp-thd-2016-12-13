#pragma once
#include "thread_safe_queue.hpp"
#include <functional>
#include <future>

using Task = std::function<void()>;

class ActiveObject
{
	ThreadSafeQueue<Task> q_;
	std::thread thd_;
	bool is_done_;
public:
	ActiveObject() : is_done_{false}
	{
		thd_ = std::thread{ [this] { run(); } };
	}

	~ActiveObject()
	{
		send([this] { is_done_ = true; });

		thd_.join();
	}

	template <typename F>
	auto send(F&& fun) -> std::future<decltype(fun())>
	{
		using Result = decltype(fun());

		auto task = std::make_shared<std::packaged_task<Result()>>(forward<F>(fun));
		std::future<Result> fresult = task->get_future();
		q_.push([task]() { (*task)(); });

		return fresult;
	}	
private:
	void run()
	{
		while (true)
		{
			Task task;			
			q_.pop(task);

			task();

			if (is_done_)
				return;
		}
	}
};
