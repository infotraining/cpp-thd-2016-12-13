#include <iostream>
#include <fstream>
#include <thread>
#include <chrono>
#include <functional>
#include <string>
#include <mutex>
#include "active_object.hpp"
#include <future>

using namespace std;

namespace Before
{

class Logger
{
	mutex mtx_;
    ofstream fout_;
public:
    Logger(const string& file_name)
    {
        fout_.open(file_name);
    }

    Logger(const Logger&) = delete;
    Logger& operator=(const Logger&) = delete;

    ~Logger()
    {
        fout_.close();
    }

    void log(const string& message)
    {
		lock_guard<mutex> lk{ mtx_ };
        fout_ << message << endl;
    }
};

}

namespace After
{
	class Logger
	{		
		ofstream fout_;
		ActiveObject ao_;
	public:
		Logger(const string& file_name)
		{
			fout_.open(file_name);
		}

		Logger(const Logger&) = delete;
		Logger& operator=(const Logger&) = delete;

		~Logger()
		{
			ao_.send([this] { fout_.close(); });
		}

		future<int> log(const string& message)
		{
			return ao_.send([=] { return do_log(message); });
		}

	protected:

		int do_log(const string& message)
		{
			fout_ << message << endl;

			return message.length();
		}
	};
}

namespace FutureDev
{
	class Document
	{
	public:
		future<void> save_async(const string& filename)
		{}

		future<string> load_from_file(const string& filename)
		{}
	};
}

using namespace After;

void run(Logger& logger, int id)
{
    for(int i = 0; i < 1000; ++i)
        logger.log("Log#" + to_string(id) + " - Event#" +to_string(i));
}

int main()
{
    /*
     * Napisz klase Logger, ktora jest thread-safe
     */

    Logger log("data.log");

    thread thd1(&run, ref(log), 1);
    thread thd2(&run, ref(log), 2);

    thd1.join();
    thd2.join();
}
