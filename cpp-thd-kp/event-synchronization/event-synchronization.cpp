#include <iostream>
#include <string>
#include <algorithm>
#include <thread>
#include <vector>
#include <numeric>
#include <atomic>
#include <condition_variable>

using namespace std;

namespace BusyWaits
{
	class Data
	{
		vector<int> data_;
		atomic<bool> is_ready_{ false };
	public:
		void read()
		{
			cout << "Start reading..." << endl;

			this_thread::sleep_for(2s);

			data_.resize(100);
			std::generate(data_.begin(), data_.end(), [] { return rand() % 100; });

			is_ready_.store(true, memory_order_release);

			cout << "End of reading..." << endl;
		}

		void process(int id)
		{
			while (!is_ready_.load(memory_order_acquire))
			{
			}

			long sum = accumulate(data_.begin(), data_.end(), 0L);

			cout << "THD#" << id << "; " << sum << endl;
		}
	};
}

namespace IdleWaits
{
	class Data
	{
		vector<int> data_;
		bool is_ready_{ false };
		mutex mtx_is_ready_;
		condition_variable cv_is_ready_;		
	public:
		void read()
		{
			cout << "Start reading..." << endl;

			this_thread::sleep_for(2s);

			data_.resize(100);
			std::generate(data_.begin(), data_.end(), [] { return rand() % 100; });

			{
				lock_guard<mutex> lk{ mtx_is_ready_ };
				is_ready_ = true;
			}

			cv_is_ready_.notify_all();

			cout << "End of reading..." << endl;
		}

		void process(int id)
		{
			unique_lock<mutex> lk{ mtx_is_ready_ };
			/*while (!is_ready_)
			{
				cv_is_ready_.wait(lk);
			}*/
			cv_is_ready_.wait(lk, [this] { return is_ready_; });

			lk.unlock(); // optional

			long sum = accumulate(data_.begin(), data_.end(), 0L);

			cout << "THD#" << id << "; " << sum << endl;
		}
	};
}

int main()
{
	IdleWaits::Data data;

	thread thd_producer{ [&data] { data.read(); } };
	thread thd_consumer1{ [&data] { data.process(1); } };
	thread thd_consumer2{ [&data] { data.process(2); } };

	thd_producer.join();
	thd_consumer1.join();
	thd_consumer2.join();

	system("PAUSE");
}