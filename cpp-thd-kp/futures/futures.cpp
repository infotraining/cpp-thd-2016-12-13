#include <iostream>
#include <string>
#include <future>
#include <random>

using namespace std;

int square(int x)
{
	cout << "square started for: " << x << endl;

	random_device rd;
	mt19937_64 rnd(rd());
	uniform_int_distribution<> distr(1000, 3000);

	this_thread::sleep_for(chrono::milliseconds(distr(rnd)));

	if (x == 13)
		throw runtime_error("Error#13");

	return x * x;
}

void save_to_file(const string& filename)
{
	cout << "saving to: " << filename << endl;

	this_thread::sleep_for(2s);
}

class SquareCalculator
{
	promise<int> promise_;
public:
	future<int> get_future()
	{
		return promise_.get_future();
	}

	void may_throw(int x)
	{
		if (x == 13)
			throw runtime_error("Error#13");
	}

	void operator()(int x)
	{
		cout << "square started for: " << x << endl;

		random_device rd;
		mt19937_64 rnd(rd());
		uniform_int_distribution<> distr(1000, 3000);

		this_thread::sleep_for(chrono::milliseconds(distr(rnd)));

		try
		{
			may_throw(x);
		}
		catch(...)
		{
			promise_.set_exception(current_exception());
			return;
		}

		promise_.set_value(x * x);
	}
};

template <typename Function>
auto spawn_task(Function&& f)
{
	using ResultType = decltype(f());
	packaged_task<ResultType()> pt{ std::forward<Function>(f) };
	auto future_result = pt.get_future();
	thread thd{ move(pt) };
	thd.detach();

	return future_result;
}

void async_wtf()
{
	async(launch::async, &save_to_file, "data1.txt");
	async(launch::async, &save_to_file, "data2.txt");
	async(launch::async, &save_to_file, "data3.txt");
}

void real_async()
{
	spawn_task([] { save_to_file("data1.txt"); });
	spawn_task([] { save_to_file("data2.txt"); });
	spawn_task([] { save_to_file("data3.txt"); });
}

int main() try
{
	// 1st way
	packaged_task<int(int)> pt1{ &square };
	future<int> f1 = pt1.get_future();
	thread thd1{ move(pt1), 8 };
	thd1.detach();

	packaged_task<int()> pt2{ [] { return square(13); } };
	future<int> f2 = pt2.get_future();
	thread thd2{ move(pt2) };
	thd2.detach();


	packaged_task<void()> pt3{ [] { save_to_file("data.txt"); } };
	auto f3 = pt3.get_future();
	thread thd3{ move(pt3) };
	thd3.detach();

	while (f2.wait_for(250ms) != future_status::ready)
	{
		cout << "still waiting for a result from f2..." << endl;
	}

	cout << "result1: " << f1.get() << endl;

	try
	{
		cout << "result2:" << f2.get() << endl;
	}
	catch(const runtime_error& e)
	{
		cout << "Caught an error: " << e.what() << endl;
	}

	f3.wait();

	// 2nd way
	SquareCalculator square_calc;
	auto f4 = square_calc.get_future();

	thread thd4{ ref(square_calc), 12 };
	thd4.detach();

	cout << "result4: " << f4.get() << endl;

	cout << "\n\n";

	// 3rd way
	auto f5 = async(launch::async, &square, 89);

	vector<future<int>> future_results;

	future_results.push_back(move(f5));

	for(int i = 1; i < 20; ++i)
	{
		future_results.emplace_back(async(launch::async, &square, i));
	}

	cout << "Results: ";
	for (auto& fr : future_results)
	{
		try
		{
			cout << fr.get() << " ";
		}
		catch(const runtime_error& e)
		{
			cout << "Caught an error: " << e.what() << endl;
		}
	}
	cout << endl;

	cout << "\n\n";
	real_async();

	system("PAUSE");
}
catch (const exception& e)
{
	cout << "Caught an error: " << e.what() << endl;
	system("PAUSE");
}