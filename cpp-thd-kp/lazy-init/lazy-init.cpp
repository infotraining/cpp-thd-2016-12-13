#include <iostream>
#include <string>
#include <thread>
#include <mutex>
#include <atomic>

using namespace std;

namespace Leaky
{
	class Singleton
	{
	private:
		static Singleton* instance_;

		Singleton()
		{
			cout << "Singleton()" << endl;
		}
	public:
		Singleton(const Singleton&) = delete;
		Singleton& operator=(const Singleton&) = delete;

		void do_stuff()
		{
			cout << "Singleton::do_stuff()" << endl;
		}

		static Singleton& instance()
		{
			if (!instance_)
				instance_ = new Singleton();

			return *instance_;
		}
	};

	Singleton* Singleton::instance_ = nullptr;
}

namespace ThreadSafeWithAtomicPointer
{
	class Singleton
	{
	private:
		static atomic<Singleton*> instance_;
		static mutex mtx_;

		Singleton()
		{
			cout << "Singleton()" << endl;
		}
	public:
		Singleton(const Singleton&) = delete;
		Singleton& operator=(const Singleton&) = delete;

		void do_stuff()
		{
			cout << "Singleton::do_stuff()" << endl;
		}

		static Singleton& instance()
		{
			if (!instance_.load())
			{
				lock_guard<mutex> lk{ mtx_ };

				if (!instance_.load())
				{
					Singleton* obj = new Singleton();
					instance_.store(obj);

					//// 1 - mem alloc
					//void* raw_mem = ::operator new(sizeof(Singleton));

					//// 2 - constructor
					//new (raw_mem) Singleton;

					//// 3 - assignment
					//instance_.store(static_cast<Singleton*>(raw_mem));
				}
			}

			return *instance_;
		}
	};

	std::atomic<Singleton*> Singleton::instance_{};
	mutex Singleton::mtx_;
}

namespace ThreadSafeWithStaticVarVS2015
{
	class Singleton
	{
	private:
		Singleton()
		{
			cout << "Singleton()" << endl;
		}
	public:
		Singleton(const Singleton&) = delete;
		Singleton& operator=(const Singleton&) = delete;

		void do_stuff()
		{
			cout << "Singleton::do_stuff()" << endl;
		}

		static Singleton& instance()
		{
			static Singleton unique_instance;
			
			return unique_instance;
		}
	};
}

namespace ThreadSafeWithStaticVarVS2013
{
	class Singleton
	{
	private:
		Singleton()
		{
			cout << "Singleton()" << endl;
		}

		static std::unique_ptr<Singleton> instance_;
		static std::once_flag init_flag_;
	public:
		Singleton(const Singleton&) = delete;
		Singleton& operator=(const Singleton&) = delete;

		void do_stuff()
		{
			cout << "Singleton::do_stuff()" << endl;
		}

		static Singleton& instance()
		{
			std::call_once(init_flag_, [] { instance_.reset(new Singleton); });			
		}
	};
}

int main()
{
	using namespace ThreadSafeWithStaticVarVS2015;

	cout << "MAIN" << endl;

	thread thd1{ [] { Singleton::instance().do_stuff(); } };

	Singleton::instance().do_stuff();

	system("PAUSE");
}