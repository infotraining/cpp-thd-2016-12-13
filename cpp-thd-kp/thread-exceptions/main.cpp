#include <iostream>
#include <string>
#include <thread>
#include <vector>
#include <algorithm>

using namespace std;

void may_throw(int id, int arg)
{
	if (arg == 13)
	{
		if (id % 2 == 0)
			throw runtime_error("Error#13 from THD#" + to_string(id));
		else
			throw invalid_argument("Invalid arg");
	}		
}

void background_work(int id, int count, chrono::milliseconds delay, exception_ptr& eptr)
{
	cout << "Start THD#" << id << endl;

	try
	{
		for (int i = 0; i < count; ++i)
		{
			cout << "THD#" << id << ": " << i << endl;
			this_thread::sleep_for(delay);
			may_throw(id, i);
		}
	}
	catch(...)
	{
		eptr = current_exception();
		cout << "Exception in THD#" << id << " stored in eptr" << endl;
	}

	cout << "End of THD#" << id << endl;
}

int main() try
{
	auto start = chrono::high_resolution_clock::now();

	exception_ptr eptr;
	thread thd1{ &background_work, 1, 20, 100ms, ref(eptr) };
	thd1.join();

	// handling an exception
	if (eptr)
	{
		try
		{
			rethrow_exception(eptr);
		}
		catch(const runtime_error& e)
		{
			cout << "Caught runtime error: " << e.what() << endl;
		}
		catch(const invalid_argument& e)
		{
			cout << "Caught invalid arg error: " << e.what() << endl;
		}
	}

	cout << "\n\n------------------------\n\n";

	const int no_of_threads = max(thread::hardware_concurrency(), 1u);

	vector<thread> thds(no_of_threads);
	vector<exception_ptr> eptrs(no_of_threads);

	for(int i = 0; i < no_of_threads; ++i)
	{
		thds[i] = thread{ &background_work, i + 2, 20, 100ms, ref(eptrs[i]) };
	}

	for (auto& t : thds)
		t.join();

	for(auto& eptr : eptrs)
	{
		if (eptr)
		{
			try
			{
				rethrow_exception(eptr);
			}
			catch (const runtime_error& e)
			{
				cout << "Caught runtime error: " << e.what() << endl;
			}
			catch (const invalid_argument& e)
			{
				cout << "Caught invalid arg error: " << e.what() << endl;
			}
		}
	}

	auto end = chrono::high_resolution_clock::now();

	cout << "Time: " 
	     << chrono::duration_cast<chrono::milliseconds>(end - start).count() << endl;

	system("PAUSE");
}
catch(const runtime_error& e)
{
	cout << "Caught an exception: " << e.what() << endl;
}