#include <iostream>
#include <string>
#include <thread>

using namespace std;

void background_work(int id, chrono::milliseconds delay)
{
	cout << "Start THD#" << id << endl;

	for (int i = 0; i < 20; ++i)
	{
		cout << "THD#" << id << ": " << i << endl;
		this_thread::sleep_for(delay);
	}

	cout << "End of THD#" << id << endl;
}

void may_throw()
{
	throw runtime_error("Error#13");
}

class ThreadJoinGuard
{
	thread& thd_;
public:
	ThreadJoinGuard(thread& thd) : thd_(thd)
	{}

	ThreadJoinGuard(const ThreadJoinGuard&) = delete;
	ThreadJoinGuard& operator=(const ThreadJoinGuard&) = delete;

	thread& get()
	{
		return thd_;
	}

	~ThreadJoinGuard()
	{
		if (thd_.joinable())
			thd_.join();
	}
};

class Forwarder
{
	string str_;
public:
	//Forwarder(string& str) : str_{str}
	//{}

	//Forwarder(const string& str) : str_{str}
	//{}

	//Forwarder(string&& str) : str_{move(str)}
	//{}

	template <
		typename Arg,
		typename = std::enable_if_t<!is_same<Forwarder, std::decay_t<Arg>>::value>
	>
	explicit Forwarder(Arg&& arg) : str_(forward<Arg>(arg))
	{
		cout << "Forwarder(T&&" << str_ << ")" << endl;
	}

	Forwarder(const Forwarder& other) : str_(other.str_)
	{
		cout << "Forwarder(cc: " << str_ << ")" << endl;
	}

	// case - 3
	/*
	template <string&&>
	Forwarder(string&& arg) : str_{ forward<string&&>(arg) }
	{
	}
	*/

};

string create_text()
{
	return "Text";
}

void use_forwarder()
{
	const string& ref_text = create_text();

	string str1 = "Hello";
	const string& str2 = "World";

	Forwarder fwd1(str1);
	Forwarder fwd2(str2);
	Forwarder fwd3(string("temp"));

	Forwarder cpy_fwd2(fwd2);
}


template <typename...> struct Typelist;

class RaiiThread
{
	thread thd_;
public:
	// perfect forwarding constructor        
	template
		<
		typename... Args,
		typename = std::enable_if_t<!is_same<Typelist<RaiiThread>, Typelist<std::decay_t<Args>...>>::value>
		>
		RaiiThread(Args&&... args) : thd_{ forward<Args>(args)... }
	{}

	RaiiThread(const RaiiThread&) = delete;
	RaiiThread& operator=(const RaiiThread&) = delete;

	RaiiThread(RaiiThread&&) = default;
	RaiiThread& operator=(RaiiThread&&) = default;

	~RaiiThread()
	{
		if (thd_.joinable())
			thd_.join();
	}

	thread& get()
	{
		return thd_;
	}
};
	

int main() try
{
	use_forwarder();

	thread thd1{ &background_work, 1, 500ms };
	ThreadJoinGuard thd1_guard{ thd1 };

	RaiiThread raii_thd2{ &background_work , 2, 250ms };

	may_throw();
}
catch(const runtime_error& e)
{
	cout << "Caught an exception: " << e.what() << endl;
}