#include <iostream>
#include <thread>
#include <mutex>

class BankAccount
{
	using mutex_t = std::recursive_mutex;

    const int id_;
    double balance_;
	mutable mutex_t mtx_;
public:
    BankAccount(int id, double balance) : id_(id), balance_(balance)
    {
    }

    void print() const
    {
        std::cout << "Bank Account #" << id_ << "; Balance = " << balance() << std::endl;
    }

    void transfer(BankAccount& to, double amount)
    {
		std::unique_lock<mutex_t> lk_from{ mtx_, std::defer_lock };
		std::unique_lock<mutex_t> lk_to{ to.mtx_, std::defer_lock };

		std::lock(lk_from, lk_to);

        //balance_ -= amount;
        //to.balance_ += amount;

		withdraw(amount); // needs recursive mutex
		to.deposit(amount);
    }

    void withdraw(double amount)
    {
		{			
			std::lock_guard<mutex_t> lk{ mtx_ }; // lock() - Acquire semantics
			
			balance_ -= amount;
		} // unlock() - Release semantics
    }

    void deposit(double amount)
    {
		std::lock_guard<mutex_t> lk{ mtx_ };
        balance_ += amount;
    }

    int id() const
    {
        return id_;
    }

    double balance() const
    {
		std::lock_guard<mutex_t> lk{ mtx_ };
        return balance_;
    }

	//void lock()
 //   {
	//	mtx_.lock();
 //   }

	//void unlock()
 //   {
	//	mtx_.unlock();
 //   }

	std::unique_lock<mutex_t> begin_transaction()
    {
		return std::unique_lock<mutex_t>{mtx_};
    }

	template <typename F>
	void process_transaction(F f)
    {
		std::lock_guard<mutex_t> lk{ mtx_ };
		f();
    }

};

void make_withdraws(size_t no_of_operations, BankAccount& ba, double amount)
{
	for(size_t i = 0; i < no_of_operations; ++i)
	{
		ba.withdraw(amount);
	}
}

void make_deposits(size_t no_of_operations, BankAccount& ba, double amount)
{
	for (size_t i = 0; i < no_of_operations; ++i)
	{
		ba.deposit(amount);
	}
}

void make_transfers(size_t no_of_operations, BankAccount& from, BankAccount& to, double amount)
{
	for (size_t i = 0; i < no_of_operations; ++i)
	{
		from.transfer(to, amount);
	}
}

int main()
{
	using namespace std;

    BankAccount ba1(1, 10000);
    BankAccount ba2(2, 10000);

    ba1.print();
    ba2.print();

	cout << "\n\n------------------\n\n";

	const size_t no_of_operations = 1'000'000;

	thread thd1{ &make_withdraws, no_of_operations, ref(ba1), 1.0 };
	thread thd2{ &make_deposits, no_of_operations, ref(ba1), 1.0 };
	thread thd3{ &make_transfers, no_of_operations, ref(ba1), ref(ba2), 1.0 };
	thread thd4{ &make_transfers, no_of_operations, ref(ba2), ref(ba1), 1.0 };

	// atomic operation
	ba1.process_transaction([&] {
		ba1.deposit(1'000'000);
		ba1.withdraw(100'000);
		ba1.transfer(ba2, 10'000);
	});

	// alt take
	{	
		auto scope_transaction = ba1.begin_transaction();
		ba1.deposit(1'000'000);
		ba1.withdraw(100'000);
		ba1.transfer(ba2, 10'000);
	}

	thd1.join();
	thd2.join();
	thd3.join();
	thd4.join();

	ba1.print();
	ba2.print();

	system("PAUSE");
}
